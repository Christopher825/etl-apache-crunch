package com.etl.crunch.service;


import com.etl.crunch.CrunchInit;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Unit test on Crunch Service Pipeline
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrunchInit.class)
public class CrunchTest {


    private static final Logger logger = LoggerFactory.getLogger(CrunchTest.class);

    @Autowired
    private EmployeeByGenderCountSvr employeeByGenderCountSvr;

    @Autowired
    private EmployeeByWorkingCountSvr employeeByWorkingCountSvr;

    @Autowired
    private AgeGroupShareAggreSvr ageGroupShareAggreSvr;

    @Autowired
    private CompensationSegmentAggreSvr compensationSegmentAggreSvr;

    @Test
    public void employeeByGenderCount() throws Exception {

        logger.debug("{}","<------Start Task - employeeByGenderCount ----->");
        logger.info("1 - {}, 0 - {}","Male","Female");
        assertThat(ToolRunner.run(new Configuration(), employeeByGenderCountSvr, null), is(equalTo(0)));
        logger.debug("{}","<------Task Done - employeeByGenderCount ----->");
    }

    @Test
    public void employeeByWorkingFuncCount() throws Exception {

        logger.debug("{}","<------Start Task - employeeByWorkingFuncCount ----->");
        assertThat(ToolRunner.run(new Configuration(), employeeByWorkingCountSvr, null), is(equalTo(0)));
        logger.debug("{}","<------Task Done - employeeByWorkingFuncCount ---->");
    }

    @Test
    public void ageGroupSharingAggre() throws Exception {

        logger.debug("{}","<------Start Task - ageGroupSharingAggre ----->");
        assertThat(ToolRunner.run(new Configuration(), ageGroupShareAggreSvr, null), is(equalTo(0)));
        logger.debug("{}","<------Task Done - ageGroupSharingAggre ---->");
    }

    @Test
    public void compensationSegmentAggre() throws Exception {

        logger.debug("{}","<------Start Task - compensationSegmentAggre ----->");
        assertThat(ToolRunner.run(new Configuration(), compensationSegmentAggreSvr, null), is(equalTo(0)));
        logger.debug("{}","<------Task Done - compensationSegmentAggre ---->");
    }



}
