package com.etl.crunch.transform;

import static org.apache.avro.generic.GenericData.*;
import org.apache.crunch.Emitter;
import org.apache.crunch.MapFn;
import org.apache.crunch.Pair;
import org.springframework.stereotype.Component;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Age Group Share Process Bean for Crunch Service
 */
@Component
public class DemoAgeGroupShare extends MapFn<Record,Pair<String,String>> {

    @Override
    public Pair<String, String> map(Record input) {
        return null;
    }

    @Override
    public void process(Record input, Emitter<Pair<String,String>> emitter) {

        emitter.emit(new Pair<>(String.valueOf(input.get("age_group")),
                String.valueOf(input.get("employee_id"))));
    }
}


