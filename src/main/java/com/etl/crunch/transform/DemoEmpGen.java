package com.etl.crunch.transform;

import static org.apache.avro.generic.GenericData.*;
import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;
import org.springframework.stereotype.Component;


/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Employee By Gender Count Process Bean for Crunch Service
 */
@Component
public class DemoEmpGen extends DoFn<Record, Integer> {

    @Override
    public void process(Record input, Emitter<Integer> emitter) {

        emitter.emit(String.valueOf(input.get("gender")).
                equalsIgnoreCase("male") ? 1 : 0);

    }
}
