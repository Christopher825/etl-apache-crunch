package com.etl.crunch.transform;


import org.apache.crunch.Emitter;
import org.apache.crunch.MapFn;
import org.apache.crunch.Pair;
import org.springframework.stereotype.Component;
import static org.apache.avro.generic.GenericData.*;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Compensation Segment Process Bean for Crunch Service
 */
@Component
public class DemoCompensationSegment extends MapFn<Record,Pair<String,Integer>> {


    @Override
    public void process(Record input, Emitter<Pair<String,Integer>> emitter) {

        emitter.emit(new Pair<>(String.join("|", String.valueOf(input.get("age_group")),
                String.valueOf(input.get("working_function")),
                String.valueOf(input.get("gender"))),
                Integer.valueOf(String.valueOf(input.get("compensation")))));
    }

    @Override
    public Pair<String, Integer> map(Record input) {
        return null;
    }
}



