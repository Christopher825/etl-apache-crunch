package com.etl.crunch.transform;

import static org.apache.avro.generic.GenericData.*;
import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;
import org.springframework.stereotype.Component;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Employee By Working Function Count Process Bean for Crunch Service
 */
@Component
public class DemoEmpWorkFunc extends DoFn<Record, String> {

    @Override
    public void process(Record input, Emitter<String> emitter) {

        emitter.emit(String.valueOf(input.get("working_function")));
    }
}

