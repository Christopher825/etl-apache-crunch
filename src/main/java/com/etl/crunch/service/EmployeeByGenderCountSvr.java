package com.etl.crunch.service;


import com.etl.crunch.transform.DemoEmpGen;
import static org.apache.crunch.Target.*;
import org.apache.avro.generic.GenericData;
import org.apache.crunch.*;
import org.apache.crunch.impl.mr.MRPipeline;
import org.apache.crunch.io.To;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import static org.apache.crunch.io.From.avroFile;
import static org.apache.crunch.types.writable.Writables.*;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Employee By Gender Count Crunch Pipeline Process
 */

@Service
public class EmployeeByGenderCountSvr extends Configured implements Tool {

    @Autowired
    private DemoEmpGen demoEmpGen;

    @Override
    public int run(String[] args) throws Exception {

        Pipeline crunch = new MRPipeline(this.getClass(),getConf());
        PCollection<GenericData.Record> recordPCollection = crunch.
                read(avroFile(new Path("input" + File.separator + "demo.avro")));
        PCollection<Integer> integerPCollection = recordPCollection.parallelDo(demoEmpGen,ints());
        PTable<Integer, Long> genderCountsTable = integerPCollection.count();
        crunch.write(genderCountsTable,To.textFile("output" + File.separator + "demoEmpGen")
                , WriteMode.OVERWRITE);
        PipelineResult pipelineResult = crunch.done();

        return pipelineResult.succeeded() ? 0 : 1;
    }
}
