package com.etl.crunch.service;

import com.etl.crunch.transform.DemoCompensationSegment;
import org.apache.avro.generic.GenericData;
import static org.apache.crunch.types.avro.Avros.*;
import org.apache.crunch.*;
import org.apache.crunch.impl.mr.MRPipeline;
import org.apache.crunch.io.To;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import java.io.File;
import static org.apache.crunch.io.From.avroFile;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Compensation Segment Aggregation of Crunch Pipeline Process
 */
@Service
public class CompensationSegmentAggreSvr extends Configured implements Tool {

    @Autowired
    private DemoCompensationSegment demoCompensationSegment;

    @Autowired
    private Environment environment;

    @Override
    public int run(String[] args) throws Exception {

        Pipeline crunch = new MRPipeline(this.getClass(), getConf());
        PCollection<GenericData.Record> recordPCollection = crunch.
                read(avroFile(new Path("input" + File.separator + "demo.avro")));
        PTable<String, Integer> psegments = recordPCollection.parallelDo(demoCompensationSegment,
                tableOf(strings(), ints()));
        crunch.write(psegments.top(Integer.valueOf(environment.getProperty("top"))),
                To.textFile("output" + File.separator + "demoCompensationSegment")
                , Target.WriteMode.OVERWRITE);
        PipelineResult pipelineResult = crunch.done();

        return pipelineResult.succeeded() ? 0 : 1;
    }
}
