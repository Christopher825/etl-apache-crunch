package com.etl.crunch.service;

import com.etl.crunch.transform.DemoAgeGroupShare;

import static org.apache.avro.generic.GenericData.*;
import static org.apache.crunch.Target.*;
import org.apache.crunch.*;
import org.apache.crunch.impl.mr.MRPipeline;
import org.apache.crunch.io.To;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import static org.apache.crunch.io.From.avroFile;
import static org.apache.crunch.types.avro.Avros.strings;
import static org.apache.crunch.types.avro.Avros.tableOf;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Age Group Sharing Aggregation of Crunch Pipeline Process
 */
@Service
public class AgeGroupShareAggreSvr extends Configured implements Tool {

    @Autowired
    private DemoAgeGroupShare demoAgeGroupShare;

    @Override
    public int run(String[] args) throws Exception {

        Pipeline crunch = new MRPipeline(this.getClass(),getConf());
        PCollection<Record> recordPCollection = crunch.
                read(avroFile(new Path("input" + File.separator + "demo.avro")));
        PTable<String, String> pageshare = recordPCollection.parallelDo(demoAgeGroupShare,
                tableOf(strings(), strings()));
        PGroupedTable<String,String> ageGroupSharePTable = pageshare.groupByKey();
        crunch.write(ageGroupSharePTable,To.textFile("output" + File.separator + "demoAgeGroupShare")
               , WriteMode.OVERWRITE);
        PipelineResult pipelineResult = crunch.done();

        return pipelineResult.succeeded() ? 0 : 1;
    }
}
