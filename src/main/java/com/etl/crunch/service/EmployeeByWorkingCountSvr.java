package com.etl.crunch.service;

import com.etl.crunch.transform.DemoEmpWorkFunc;

import static org.apache.crunch.Target.*;
import org.apache.avro.generic.GenericData;
import org.apache.crunch.PCollection;
import org.apache.crunch.PTable;
import org.apache.crunch.Pipeline;
import org.apache.crunch.PipelineResult;
import org.apache.crunch.impl.mr.MRPipeline;
import org.apache.crunch.io.To;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import static org.apache.crunch.io.From.avroFile;
import static org.apache.crunch.types.writable.Writables.*;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Employee By Working Function Count of Crunch Pipeline Process
 */
@Service
public class EmployeeByWorkingCountSvr extends Configured implements Tool {

    @Autowired
    private DemoEmpWorkFunc demoEmpWorkFunc;


    @Override
    public int run(String[] args) throws Exception {

        Pipeline crunch = new MRPipeline(this.getClass(),getConf());
        PCollection<GenericData.Record> recordPCollection = crunch.
                read(avroFile(new Path("input" + File.separator + "demo.avro")));
        PCollection<String> integerPCollection = recordPCollection.parallelDo(demoEmpWorkFunc,strings());
        PTable<String, Long> workingFuncCountsTable = integerPCollection.count();
        crunch.write(workingFuncCountsTable, To.textFile("output" + File.separator + "demoEmpWorkFunc"),
                WriteMode.OVERWRITE);
        PipelineResult pipelineResult = crunch.done();

        return pipelineResult.succeeded() ? 0 : 1;
    }
}
