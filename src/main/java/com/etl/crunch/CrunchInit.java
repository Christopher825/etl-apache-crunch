package com.etl.crunch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by christopherloganathan on 11/05/2017.
 * Purpose: Boot Application of Crunch Service
 */
@SpringBootApplication
public class CrunchInit {

    public static void main(String... args){

        SpringApplication.run(CrunchInit.class, args);

    }
}
