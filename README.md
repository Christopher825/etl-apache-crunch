### Apache Crunch ETL Mini Microservice Backend ###

# Development technologies #

* JDK 1.8.X_XX
* Spring Boot v1.4.0.RELEASE
* Spring v4.3.2.RELEASE
* Spring Boot Test
* org.apache.crunch:crunch-core:0.15.0
* org.apache.servicemix.bundles:org.apache.servicemix.bundles.hadoop-client: 2.4.1_1
* Gradle plugin v1.4.0.RELEASE
* Gradle v3.5

#Run test#

1) Go into the etl-apache-crunch folder after git clone the project and execute below scripts:-

**For Windows user:-**

a) gradlew.bat cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.employeeByGenderCount

b) gradlew.bat cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.employeeByWorkingFuncCount

c) gradlew.bat cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.ageGroupSharingAggre

d) gradlew.bat cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.compensationSegmentAggre

**For Unix user:-**

a) ./gradlew cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.employeeByGenderCount

b) ./gradlew cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.employeeByWorkingFuncCount

c) ./gradlew cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.ageGroupSharingAggre

d) ./gradlew cleanTest test -Penv=test --info --tests com.etl.crunch.service.CrunchTest.compensationSegmentAggre

2) Input file which you had given "demo.avro" file can be found in :-

etl-apache-crunch/input/*

3) Output files of the pipeline processed results can be found in:-

**Note: Write Mode is Overwrite.**

etl-apache-crunch/output/**

4) Finally, to reassign with the new top segments settings before re-executing script (d), this settings can be altered in:-

etl-apache-crunch/config/application-test.properties


# Who do I talk to? #

* Admin : Christopher Loganathan
* Email Contact : christopherloganathan@gmail.com